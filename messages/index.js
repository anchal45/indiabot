// "use strict";
var builder = require("botbuilder");
const uuid = require('uuid');
// var botbuilder_azure = require("botbuilder-azure");
// var path = require('path');
// var apiairecognizer = require('api-ai-recognizer');
// var useEmulator = (process.env.NODE_ENV == 'development');
var apiai = require('apiai');
var app = apiai("a7be727810f34fa5ab01d6b41fe35e77");
require('../common_res_en.js')();

// var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
//     appId: process.env['MicrosoftAppId'],
//     appPassword: process.env['MicrosoftAppPassword'],
//     stateEndpoint: process.env['BotStateEndpoint'],
//     openIdMetadata: process.env['BotOpenIdMetadata']
// });

// var bot = new builder.UniversalBot(connector);
// bot.localePath(path.join(__dirname, './locale'));

//  var recognizer = new apiairecognizer('a7be727810f34fa5ab01d6b41fe35e77');
//    var intents = new builder.IntentDialog({
//          recognizers: [recognizer]
//     });

//    bot.dialog('/',intents);

//     intents.matches('STATS_capital_of_india',function(session){
//       session.send("Delhi");
//    });

//    intents.onDefault(function(session){
//       session.send("Sorry...can you please rephrase?");
//    });

/*bot.dialog('/', function (session) {
    session.send('You said ' + session.message.text);
});
*/
// if (useEmulator) {
var restify = require('restify');
var server = restify.createServer();
var result;
var answer;
server.listen(3278, function () {
    console.log('test bot endpoInt at http://localhost:3278/api/messages');
});

var connector = new builder.ChatConnector({
    appId: '44c17d70-aed9-45a1-892a-1e7bf19506ba',
    appPassword: 'Ek7Ukx7sv3zepNJTBLKDj0q'
});

server.post('/api/messages', connector.listen());

var bot = new builder.UniversalBot(connector);


//Local variables
var context = {};


bot.dialog('/', function (session) {
    var intent = {
        score: 0.0
    };
    var sessionId = uuid();
    var request = app.textRequest(session.message.text, {
        sessionId: sessionId
    });

    request.on('response', function (response) {
        if (response.result.metadata.intentName) {
            result = response.result.metadata.intentName;

            //            context.entity =
            if (result == 'DYNA_min_resource_need' || result == 'DYNA_old_technology_work') {
                //answer = "Intent:" + result;
                context.intent = result;
                var answer = new builder.Message().attachmentLayout(builder.AttachmentLayout.carousel).attachments([
                    new builder.HeroCard(session)

                        .text(resource(result))
                        .buttons([builder.CardAction.postBack(session, "See reference", "Reference"),
                                 builder.CardAction.postBack(session, "See Contacts", "Contacts")])
                    ]);
                session.send(answer);
                //session.beginDialog('/reference');
            } else if (result == 'DYNA_reference') {
                if (context.intent == 'DYNA_min_resource_need') {
                    result = 'DYNA_min_resource_need_reference';
                    answer = resource(result);
                    session.send(answer);
                } else if (context.intent == 'DYNA_old_technology_work') {
                    result = 'DYNA_old_technology_work_reference';
                    answer = resource(result);
                    session.send(answer);
                }
            } else if (result == 'DYNA_contacts') {
                if (context.intent == 'DYNA_min_resource_need') {
                    result = 'DYNA_min_resource_need_contacts';
                    answer = resource(result);
                    session.send(answer);
                } else if (context.intent == 'DYNA_old_technology_work') {
                    result = 'DYNA_old_technology_work_contacts';
                    answer = resource(result);
                    session.send(answer);
                }
            } else if (result == 'STATS_books_about_India') {
                answer = new builder.Message(session);
                answer.attachmentLayout(builder.AttachmentLayout.carousel)
                answer.attachments([
                    new builder.HeroCard(session)
                        .title(" Discovery of India")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/discovery_of_india.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.goodreads.com/book/show/154126.The_Discovery_of_India", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("IN SPITE OF GODS :THE STRANGE RISE OF MODERN INDIA  By Edward Luce")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/in-spite-of-gods.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.guardian.co.uk/books/2006/aug/20/shopping.society", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("A PASSAGE TO INDIA – By E.M.Forster")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/a-passage-to-india72.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.goodreads.com/book/show/45195.A_Passage_to_India", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("INDIA AFTER GANDHI – Ramachandra Guha")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/india-after-gandhi.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.guardian.co.uk/books/2007/apr/21/featuresreviews.guardianreview3", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("INDIA – A WOUNDED CIVILIZATION – By V.S. Naipaul")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/wounded-civilization.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.goodreads.com/book/show/5847.India", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("INDIA – A PORTRAIT  – By Patrick French")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/india_a-portrait.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.guardian.co.uk/books/2011/jan/16/patrick-french-india-aravind-adiga", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("THE WONDER THAT WAS INDIA – By A.L. Basham")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/the-wonder.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.goodreads.com/book/show/631246.The_Wonder_That_Was_India", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("AUTOBIOGRAPHY OF AN UNKNOWN INDIAN – By Nirad C. Chaudhuri")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/the-autobiography-of-an-unknown-indian.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://en.wikipedia.org/wiki/Mark_Tully", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("INDIA’S UNENDING JOURNEY – By Mark Tully")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/indias-unedning-journey.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.independent.co.uk/travel/asia/mark-tully-my-unending-journey-through-india-446739.html", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("THE WORLD IS FLAT – By Thomas Friedman")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/the_world_is_flat.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.nytimes.com/2005/05/01/books/review/01ZAKARIA.html?pagewanted=all", "Book review")])
                    ,
                    new builder.HeroCard(session)
                        .title("INDIA UNBOUND – THE SOCIAL AND ECONOMIC REVOLUTION FROM INDEPENDENCE TO THE GLOBAL INFORMATION AGE – By Gurcharan Das")
                        .images([builder.CardImage.create(session, 'https://sopraevents.blob.core.windows.net/ssifaqbot/india-unbound.jpg')])
                        .buttons([builder.CardAction.openUrl(session, "http://www.guardian.co.uk/books/2006/aug/20/shopping.society", "Book review")])
                ]);
                session.send(answer);
            } else if (result.startsWith('STATS_')) {
                answer = resource(result);
                session.send(answer);
            }
            //   else if(result.startsWith('DYNA_')){
            //     session.beginDialog('/reference');
            //   }
            // }
        } else if (response.result.action) {
            result = response.result.action.startsWith("smalltalk");
            answer = response.result.fulfillment.messages[0].speech;
            session.send(answer);
        }


        // session.send(result);
    });


    request.on('error', function (response) {
        console.log(error);
    });

    request.end();
    // intent = {score: result.score,intent: result.metadata.intentName};
    // session.send("Intent: "+intent);

});

//bot.dialog('/reference', function (session, result) {
//    var sessionId = uuid();
//    var request = app.textRequest(session.message.text, {
//        sessionId: sessionId
//    });
//
//    request.on('response', function (response) {
//        if (response.result.metadata.intentName == 'DYNA_min_resource_need_reference') {
//            answer = "context:reference";
//        }
//    });
//    session.send(answer);
//    session.endDialog();
//});

bot.dialog('/test', function (session) {
    session.send('anchal is very bad');
}).triggerAction({
    matches: 'DYNA_old_technology_reference'
});

//function(session,result){
// intents.matches('STATS_capital_of_india',function(session){
//   session.send("session.message.text: "+session.message.text);
// });



//     , function (session) {
//     session.send("You said: %s", session.message.text);
//     });
// // } else {
//     module.exports = { default: connector.listen() }
// }
